import re
import json
import PyPDF2
from pdfminer.high_level import extract_text
import shutil
from nltk.corpus import stopwords
from xml.dom import minidom

stop = stopwords.words('english')

regex = r"([a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+)"

data = []

from ..grobid_client_python.grobid_client.grobid_client import GrobidClient
client = GrobidClient(config_path="./grobid_client_python/config.json")


class EmailService:

    def __init__(self):
        self.encoding = 'utf-8'

    def get_file_ext(self, file_name):
        tokens = file_name.split(".")
        ext = None
        if len(tokens) > 0:
            ext = tokens[-1].lower()
        return ext

    def convert_pdf_to_text(self, file, UploadPdf):
        ext = "pdf"

        if UploadPdf == 'True':
            #print(file)
            with open("temporaryfile/output.pdf", "wb") as buffer:
                shutil.copyfileobj(file.file, buffer)

            file_name = "temporaryfile/output.pdf"
        else:
            file_name = "temporaryfile/output.pdf"

        if ext in ["pdf"]:
            text = extract_text(file_name)
            full_text = [text]
            with open(file_name, 'rb') as f:
                reader = PyPDF2.PdfFileReader(f)
                for pageNumber in range(reader.numPages):
                    page = reader.getPage(pageNumber)
                    try:
                        txt = page.extractText()
                        full_text.append(txt)

                    except Exception:
                        print("Error PDF reader ", file_name, pageNumber)

            text_paper = '\n'.join(full_text)
            return text_paper

        else:
            return "Unknown file extension."

    def get_email(self, email_list):
        emails = []
        #print(email_list)
        for email in email_list:
            if '{' or '}' in email:
                email_head = email.split("@")[0]
                email_tail = email.split("@")[1]
                email_names = email_head.replace('{', '').replace('}', '').split(",")
                for email_name in email_names:
                    emails.append(email_name + '@' + email_tail)
            else:
                emails.append(email)
        return emails

    def get_emails_from_pdf(self, fileName, UploadPdf):
        try:
            txt = self.convert_pdf_to_text(fileName, UploadPdf)
            email_list_all = re.findall(r"([a-zA-Z0-9_.+-{}]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+)", txt)
            email_list = []
            for email in email_list_all:
                if email not in email_list:
                    if email[0].isupper() == False:
                        email_list.append(email)

            all_email_ = self.get_email(email_list)
            return list(all_email_)

        except Exception as e:
            return {"message": "Getting error from PDF."}


    def convert_pdf(self,UploadFile, UploadPdf):
        client.process("processFulltextDocument", "./temporaryfile/", output="./temporaryfile/", consolidate_citations=True, tei_coordinates=True, force=True)



    def get_emails_from_grobid(self, UploadFile, UploadPdf):
        email_list = []
        self.convert_pdf(UploadFile,UploadPdf)
        file = minidom.parse('temporaryfile/output.tei.xml')
        emails = file.getElementsByTagName('email')
        for i in range(0,len(emails),1):
            email_list.append(emails[i].firstChild.data)
        return email_list


    def merge_grobid_and_ragex(self, fileName, UploadPdf):
        data = {}
        from_pdf = self.get_emails_from_pdf(fileName, UploadPdf)
        from_grobid = self.get_emails_from_grobid(fileName, UploadPdf)

        all_email_ =from_grobid.copy()

        for i in range(0, len(from_pdf), 1):
            if from_pdf[i] not in all_email_:
                all_email_.append(from_pdf[i])

        for i in range(0, len(all_email_), 1):
            full_name = re.sub(r'[^\w\s]', ' ', all_email_[i].split("@")[0])
            if len(full_name) > 1:
                if len(all_email_[i].split("@")[0])<30:
                    if len(all_email_[i].split("@")[1]) < 20:
                        data[full_name] = all_email_[i].split(",")[0]

        return json.dumps(data)
