import requests
import json


class ORCIDEmailService:

    def __init__(self):
        self.encoding = 'utf-8'

    def GetCredentialsFromORCID(self, _id):
        data = {}
        request = requests.get(f'https://pub.orcid.org/v3.0/expanded-search/?start=0&rows=200&q=orcid:{_id}',
                               headers={'User-Agent': 'Mozilla/5.0', 'accept': 'application/json'})
        try:
            r_json = json.loads(request.text)
            email_data = r_json['expanded-result'][0]['email']
            name = str(r_json['expanded-result'][0]['given-names'])+" "+str(r_json['expanded-result'][0]['family-names'])
            full_name = name.encode(encoding="ascii",errors="ignore").decode("utf-8")
            for i in range(0, len(email_data), 1):
                data[full_name] = email_data[i]

            if len(data) != 0:
                json_data = json.dumps(data)
                return json_data
            else:
                return "The researcher has no email id in ORCID."

        except Exception as e:
            return "Getting error from ORCID."
