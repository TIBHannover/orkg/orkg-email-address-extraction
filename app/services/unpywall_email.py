from unpywall.utils import UnpywallCredentials
from unpywall import Unpywall
import requests


class UnpaywallEmailService:

    def __init__(self):
        self.encoding = 'utf-8'

    def GetCredentialsFromUnpywall(self):
        UnpywallCredentials('nick.haupka@gmail.com')

    def GetUnpywallPdf(self, doi):
        UnpywallCredentials('nick.haupka@gmail.com')
        try:
            pdf_link = Unpywall.get_pdf_link(doi=doi)
            return pdf_link
        except Exception as e:
            return None

    def StorePdf(self, pdf_link):
        file_name = "temporaryfile/output.pdf"
        try:
            headers = {
                "User-Agent": "Chrome/51.0.2704.103",
            }
            response = requests.get(pdf_link, headers=headers)
            # Save the PDF
            if response.status_code == 200:
                with open(file_name, "wb") as f:
                    f.write(response.content)
                return file_name
            else:
                return response.status_code
        except Exception as e:
            return "Getting error from Unpaywall."
