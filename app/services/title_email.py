import requests
import urllib.parse

class TitleEmailService:

    def __init__(self):
        pass

    def GetCrossrefDOI(self, title):
        url_encoded_title = urllib.parse.quote_plus(title)
        url = 'https://api.crossref.org/works?rows=1&query.bibliographic={}'.format(url_encoded_title) #, headers=self.headers, params=self.params

        response = requests.get(url)
        if not response.ok:
            self.logger.warning('Request error returns response: {}'.format(response.__dict__))
        response = response.json()
        if 'items' in response['message']:
            for item in response['message']['items']:
                return item['DOI']
                break
