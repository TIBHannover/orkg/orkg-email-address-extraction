import sys


sys.path.append("..")

from fastapi import APIRouter, UploadFile, Form, File
from fastapi.responses import HTMLResponse

from app.common.util.decorators import log
from app.services.unpywall_email import UnpaywallEmailService
from app.services.title_email import TitleEmailService
from app.services.email import EmailService


router = APIRouter(
    prefix='/email',
    tags=['email']
)


@router.post('/email_extraction_using_title', response_class=HTMLResponse, status_code=200)
@log(__name__)
def email_extraction_from_pdf_using_Unpaywall(title, UploadPdf):
    DOI_service = TitleEmailService()
    email_service = UnpaywallEmailService()
    email_from_pdf = EmailService()

    doi = DOI_service.GetCrossrefDOI(title)
    pdf_link = email_service.GetUnpywallPdf(doi)
    if pdf_link == None:
        return {"message": "Getting no pdf file from Unpaywall."}
    else:
        file = email_service.StorePdf(pdf_link)
        return email_from_pdf.merge_grobid_and_ragex(file, UploadPdf)
