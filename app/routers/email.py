import sys

sys.path.append("..")

from fastapi import APIRouter, UploadFile, Form, File
from fastapi.responses import HTMLResponse

from app.common.util.decorators import log
from app.services.email import EmailService

router = APIRouter(
    prefix='/email',
    tags=['email']
)


@router.post('/email_extraction_pdf', response_class=HTMLResponse, status_code=200)
@log(__name__)
def email_extraction(file: UploadFile, UploadPdf):
    email_service = EmailService()
    return email_service.merge_grobid_and_ragex(file, UploadPdf)
