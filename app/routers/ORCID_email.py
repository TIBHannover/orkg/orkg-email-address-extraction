import sys

sys.path.append("..")

from fastapi import APIRouter, UploadFile, Form, File
from fastapi.responses import HTMLResponse

from app.common.util.decorators import log
from app.services.ORCID_email import ORCIDEmailService

router = APIRouter(
    prefix='/email',
    tags=['email']
)


@router.post('/email_extraction_ORCID', response_class=HTMLResponse, status_code=200)
@log(__name__)
def email_extraction_ORCID(ORCID_id):
    email_service = ORCIDEmailService()
    return email_service.GetCredentialsFromORCID(ORCID_id)
