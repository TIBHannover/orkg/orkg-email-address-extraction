import json
import sys
sys.path.append("..")

from fastapi import APIRouter, UploadFile, Form, File
from fastapi.responses import HTMLResponse

from app.common.util.decorators import log
from app.services.email import EmailService
from app.services.unpywall_email import UnpaywallEmailService


router = APIRouter(
    prefix='/email',
    tags=['email']
)


@router.post('/email_extraction_using_doi', response_class=HTMLResponse, status_code=200)
@log(__name__)
def email_extraction_from_pdf_using_Unpaywall(doi, UploadPdf):
    email_service = UnpaywallEmailService()
    email_from_pdf = EmailService()
    pdf_link = email_service.GetUnpywallPdf(doi)
    if pdf_link == None:
        return "Getting no pdf file from Unpaywall."

    else:
        file = email_service.StorePdf(pdf_link)
        return email_from_pdf.merge_grobid_and_ragex(file, UploadPdf)