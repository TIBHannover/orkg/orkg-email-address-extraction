import pandas as pd
import requests

def test_with_doi(df):

    match = 0
    status_error = 0

    for row in range(0,rows,1):
        doi = df.Link[row]
        if len(doi.split("/")) == 5:
            api_url_base = 'http://127.0.0.1:8000/email/email_extraction_using_doi?doi=' + str(doi) + '&UploadPdf=False'
            response = requests.post(api_url_base)

            if response.status_code == 200:
                for key, value in response.json().items():
                    if value in df.EMail[row]:
                        match = match+1
            else:
                status_error = status_error+1

    print(match)
    print(status_error)

def test_with_title(df):

    match = 0
    status_error = 0

    for row in range(0,rows,1):
        title = df['Paper (informal citation)'][row]
        api_url_base = 'http://127.0.0.1:8000/email/email_extraction_using_title?title=' + str(title) + '&UploadPdf=False'
        response = requests.post(api_url_base)

        if response.status_code == 200:
            for key, value in response.json().items():
                if value in df['Author e-mail'][row]:
                        match = match+1
        else:
            status_error = status_error+1

    print(match)
    print(status_error)


path = 'Email_dataset/CS-NER-User study_AuthorList.csv'
df = pd.read_csv(path, error_bad_lines=False)
rows, cols = df.shape
print(rows)
test_with_title(df)
